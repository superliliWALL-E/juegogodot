extends Node2D

# Declare member variables here. Examples:
var inicio = Vector2()
var final = Vector2()
var Esvisible = false

# Called when the node enters the scene tree for the first time.
func _input(event):
	if event is InputEventMouseButton:
		if Input.is_action_just_pressed("seleccionar"):
			print("Mouse Click at: ", get_local_mouse_position())
			inicio = get_local_mouse_position()
			$verde.visible = true
			Esvisible = true
		elif Input.is_action_just_released("seleccionar"):
			print("Mouse Unclick at: ", get_local_mouse_position())
			$verde.visible = false
			Esvisible = false
	if InputEventMouseMotion:
		print("Mouse is at: ", get_local_mouse_position())
		if Esvisible:
			final = get_local_mouse_position();
			$verde.position = inicio+(final-inicio)
			$verde.scale.x = -(final.x - inicio.x)
			$verde.scale.y = -(final.y - inicio.y) 
			